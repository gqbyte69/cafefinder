//
//  CFPlaceCell.swift
//  CafeFinder
//
//  Created by Adonis Dumadapat on 08/01/2018.
//  Copyright © 2018 A3T Software Development Services. All rights reserved.
//

import UIKit
import SDWebImage

class CFPlaceCell: UICollectionViewCell {

    @IBOutlet weak var imgPlace: UIImageView!
    @IBOutlet weak var imgBG: CFImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var viewContainer: UIView!

    var item: CFPlaceModel! {
        didSet {
            lblName.text = item.name
            lblAddress.text = item.vicinity

            imgPlace.sd_setShowActivityIndicatorView(true)
            imgPlace.sd_setIndicatorStyle(.gray)

            if item.photos.count > 0 {
                let imgUrl: String = item.photos.first!.photUrl(withWidth: 110)

                // Load image from url
                imgPlace.sd_setImage(
                    with: NSURL(string: imgUrl)! as URL,
                    placeholderImage: IMG.PLACEHOLDER,
                    options: .cacheMemoryOnly,
                    progress: nil) { (image, error, type, url) in
                        self.imgBG.image = image!
                        self.imgBG.blurEffect()
                }
            } else {
                imgPlace.image = IMG.PLACEHOLDER
            }
        }
    }

    override func awakeFromNib() {
        viewContainer.addShadow()
        viewContainer.cornerRadius = 5
    }
}
