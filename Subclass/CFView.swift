//
//  CFView.swift
//  CafeFinder
//
//  Created by Adonis Dumadapat on 06/01/2018.
//  Copyright © 2012-2018 A3T Software Development Services, Inc.
//

import UIKit

class CFView: UIView {

    typealias DidTapView = (CFView) -> ()
    internal var tapRecognizer: UITapGestureRecognizer!

    var didTouchUpInside: DidTapView? {
        didSet {
            if didTouchUpInside != nil {
                addGestureRecognizer(tapRecognizer)
            } else {
                removeGestureRecognizer(tapRecognizer)
            }
        }
    }

    var isSelected: Bool = false {
        didSet {
            if isSelected {
                self.borderWidth = 1.0
            } else {
                self.borderWidth = 0
            }
        }
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTouchUpInside(sender:)))
    }


    // MARK: - Actions
    @objc func didTouchUpInside(sender: UIView) {
        if let handler = didTouchUpInside {
            handler(self)
        }
    }
}
