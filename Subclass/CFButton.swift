//
//  CFButton.swift
//  CafeFinder
//
//  Created by Adonis Dumadapat on 06/01/2018.
//  Copyright © 2012-2018 A3T Software Development Services, Inc.
//

import UIKit

class CFButton: UIButton {
    
    typealias DidTapButton = (CFButton) -> ()

    var layerGradient: CAGradientLayer!
    var gradientGreen: [Any] =
    [
        UIColor.rgb(red: 1, green: 254, blue: 1).cgColor,
        UIColor.rgb(red: 8, green: 99, blue: 8).cgColor
    ]
    var gradientRed: [Any] =
    [
        UIColor.rgb(red: 254, green: 1, blue: 1).cgColor,
        UIColor.rgb(red: 99, green: 8, blue: 8).cgColor
    ]

    @IBInspectable var isTitleUndelined: Bool {
        set {
            if newValue {
                let attr = NSAttributedString(string: (self.titleLabel?.text)!, attributes: [NSAttributedStringKey.underlineStyle : 1])
                self.setAttributedTitle(attr, for: .normal)
            }
        }
        get {
            return true
        }
    }
    
    var didTouchUpInside: DidTapButton? {
        didSet {
            if didTouchUpInside != nil {
                addTarget(self, action: #selector(didTouchUpInside(sender:)), for: .touchUpInside)
            } else {
                removeTarget(self, action: #selector(didTouchUpInside(sender:)), for: .touchUpInside)
            }
        }
    }
    
    // MARK: - Actions
    @objc func didTouchUpInside(sender: UIButton) {
        if let handler = didTouchUpInside {
            handler(self)
        }
    }

    func addGradientColor(color: GRADIENT_COLORS) {
        layerGradient = CAGradientLayer.init()
        layerGradient.frame = bounds

        switch color {
        case .GREEN:
            layerGradient.colors = gradientGreen
            break
        case .RED:
            layerGradient.colors = gradientRed
            break
        }

        layer.insertSublayer(layerGradient, at: 0)
        layer.masksToBounds = true
        layer.cornerRadius = 5
        layer.borderWidth = 1
        layer.borderColor = UIColor.black.cgColor
    }
}

