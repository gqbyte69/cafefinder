//
//  CFDashboardViewController.swift
//  CafeFinder
//
//  Created by Adonis Dumadapat on 06/01/2018.
//  Copyright © 2018 A3T Software Development Services. All rights reserved.
//

import UIKit
import CoreLocation

class CFDashboardViewController: CFBaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, CLLocationManagerDelegate, UISearchBarDelegate {

    @IBOutlet weak var cvPlaces: UICollectionView!
    @IBOutlet weak var txtSearch: UISearchBar!

    var arrayPlaces: [CFPlaceModel] = []
    var currentLocation: CLLocation!
    var locationManager: CLLocationManager = CLLocationManager()
    var initialLoaded: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()

        // Ask for authorization
        locationManager.requestAlwaysAuthorization()

        locationManager.requestWhenInUseAuthorization()

        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }

        title = APP_NAME
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }

    func searchNearby(withKeyword keyword: String? = nil) {
        CFApiWrapper.getNearby(
            withKeyword: keyword,
            withCoordinate: currentLocation) { (status, message, data) in
                if !status {
                    CFAlert.error(withTitle: message)
                } else {
                    self.arrayPlaces.removeAll()
                    self.arrayPlaces = data as! [CFPlaceModel]
                    self.cvPlaces.reloadData()
                }
        }
    }

    //MARK: - UISearchBarDelegate -
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {

        if searchBar.text != "" {
            view.endEditing(true)
            searchNearby(withKeyword: searchBar.text)
        }
    }

    //MARK: - CLLocationManagerDelegate -
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currentLocation = manager.location

        if !initialLoaded {
            initialLoaded = true

            self.searchNearby()
        }
    }

    //MARK: - UICollectionViewDataSource -
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayPlaces.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CFPlaceCellID", for: indexPath) as! CFPlaceCell
        let place: CFPlaceModel = arrayPlaces[indexPath.row]

        cell.item = place

        // Calculate and display current distance in meters
        if let location = currentLocation {
            let distance = location.distance(from: place.geometry.location)
            cell.lblDistance.text = String(format: "%.2f meters", distance)
        } else {
            cell.lblDistance.text = "Unknown distance"
        }

        return cell
    }

    //MARK: - UICollectionViewDelegate -
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let place: CFPlaceModel = arrayPlaces[indexPath.row]

        let vc: CFPlaceDetailsViewController = VC().DETAILS
        vc.place = place

        if let location = currentLocation {
            let distance = location.distance(from: place.geometry.location)
            vc.distanceString = String(format: "%.2f meters away from you.", distance)
        } else {
            vc.distanceString = "Unknown distance"
        }

        pushVC(viewController: vc)
    }

}
