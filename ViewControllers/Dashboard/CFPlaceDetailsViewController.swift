//
//  CFPlaceDetailsViewController.swift
//  CafeFinder
//
//  Created by Adonis Dumadapat on 08/01/2018.
//  Copyright © 2018 A3T Software Development Services. All rights reserved.
//

import UIKit
import MapKit
import SDWebImage

class CFPlaceDetailsViewController: CFBaseViewController {

    @IBOutlet weak var mvLocation: MKMapView!
    @IBOutlet weak var imgPlace: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblDistance: UILabel!

    @IBOutlet weak var imgBg: CFImageView!

    var place: CFPlaceModel!
    var distanceString: String!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Add navbar title
        title = place.name

        // Add activity indicator on image view
        imgPlace.sd_setShowActivityIndicatorView(true)
        imgPlace.sd_setIndicatorStyle(.gray)

        if place.photos.count > 0 {
            // Get width of the screen
            let width: CGFloat = UIScreen.main.bounds.width
            let imgUrl: String = place.photos.first!.photUrl(withWidth: width)

            // Load image from url
            imgPlace.sd_setImage(
                with: NSURL(string: imgUrl)! as URL,
                placeholderImage: IMG.PLACEHOLDER,
                options: .cacheMemoryOnly,
                progress: nil) { (image, error, type, url) in
                    self.imgBg.image = image!
                    self.imgBg.blurEffect()
            }
        } else {
            imgPlace.image = IMG.PLACEHOLDER
        }

        // Show region of currently selected place
        let viewRegion = MKCoordinateRegionMakeWithDistance(place.geometry.location.coordinate, 200, 200)
        mvLocation.setRegion(viewRegion, animated: true)

        // Add a pin to the selected place
        let annotation = MKPointAnnotation()
        annotation.coordinate = place.geometry.location.coordinate
        mvLocation.addAnnotation(annotation)

        // Assign values to detail labels
        lblName.text = place.name
        lblAddress.text = place.vicinity
        lblDistance.text = distanceString
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
}
