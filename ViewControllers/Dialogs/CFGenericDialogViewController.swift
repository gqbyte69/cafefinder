//
//  CFGenericDialogViewController.swift
//  CafeFinder
//
//  Created by Adonis Dumadapat on 06/01/2018.
//  Copyright © 2012-2018 A3T Software Development Services, Inc.
//

import UIKit
import Foundation
import PopupDialog

class CFGenericDialogViewController: CFBaseViewController {

    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMainSubtitle: UILabel!
    @IBOutlet weak var lblSubSubtitle: UILabel!
    @IBOutlet weak var btnDestructive: CFButton!
    @IBOutlet weak var btnDefault: CFButton!
    @IBOutlet weak var btnDefaultOnly: CFButton!

    @IBOutlet weak var constHeightIconImage: NSLayoutConstraint!
    @IBOutlet weak var constHeightTitle: NSLayoutConstraint!
    @IBOutlet weak var constHeightMainSubtitle: NSLayoutConstraint!
    @IBOutlet weak var constHeightSubSubtitle: NSLayoutConstraint!

    @IBOutlet weak var constBottomSpaceImage: NSLayoutConstraint!
    @IBOutlet weak var constBottomSpaceTitle: NSLayoutConstraint!
    @IBOutlet weak var constBottomSpaceMainSubtitle: NSLayoutConstraint!
    @IBOutlet weak var constBottomSpaceSubtitle: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

    class func showDialog(inVC : UIViewController,
                          iconImage : UIImage? = nil,
                          title : String?,
                          defaultOnly : Bool? = false,
                          mainSubtitle : String?,
                          subSubtitle : String?,
                          defaultTitle : String?,
                          destructiveTitle : String?,
                          dismissAction: (() -> Swift.Void)? = nil,
                          onPresentComplete: (() -> Swift.Void)?,
                          defaultAction: (() -> Swift.Void)?,
                          destructiveAction: (() -> Swift.Void)?)
    {

        // Instantiate the view controller
        let thisVC = CFGenericDialogViewController(nibName: "CFGenericDialogViewController", bundle: nil)
        thisVC.view.layoutIfNeeded()
        thisVC.view.backgroundColor = UIColor.clear
        thisVC.viewContainer.layer.cornerRadius = 10.0

        // Customize the buttons
        thisVC.btnDefault.addGradientColor(color: .GREEN)
        thisVC.btnDefaultOnly.addGradientColor(color: .GREEN)
        thisVC.btnDestructive.addGradientColor(color: .RED)

        // Create the dialog
        let popup = PopupDialog(viewController: thisVC, buttonAlignment: .horizontal, transitionStyle: .bounceDown, gestureDismissal: false) {
            dismissAction?()
        }

        popup.view.backgroundColor = UIColor.clear

        // Hide show buttons
        if defaultOnly! {
            thisVC.btnDefault.isHidden = true
            thisVC.btnDestructive.isHidden = true
            thisVC.btnDefaultOnly.isHidden = false
        } else {
            thisVC.btnDefault.isHidden = false
            thisVC.btnDestructive.isHidden = false
            thisVC.btnDefaultOnly.isHidden = true
        }

        // Assign button actions
        thisVC.btnDefault.didTouchUpInside = { (sender) in
            popup.dismiss(animated: true, completion: {
                defaultAction?()
            })
        }

        thisVC.btnDefaultOnly.didTouchUpInside = { (sender) in
            popup.dismiss(animated: true, completion: {
                defaultAction?()
            })
        }

        thisVC.btnDestructive.didTouchUpInside = { (sender) in
            popup.dismiss(animated: true, completion: {
                destructiveAction?()
            })
        }

        // Changes the height constraints if params are nil
        if iconImage == nil {
            thisVC.constHeightIconImage.constant = 0
            thisVC.constBottomSpaceImage.constant = 0
        } else {
            thisVC.imgIcon.image = iconImage
        }

        if title != nil {
            thisVC.lblTitle.text = title?.trimmingCharacters(in: .whitespaces)
        } else {
            thisVC.lblTitle.isHidden = true
            thisVC.constHeightTitle.constant = 0
            thisVC.constBottomSpaceTitle.constant = 0
        }

        if mainSubtitle != nil {
            thisVC.lblMainSubtitle.text = mainSubtitle?.trimmingCharacters(in: .whitespaces)
        } else {
            thisVC.lblMainSubtitle.isHidden = true
            thisVC.constHeightMainSubtitle.constant = 0
            thisVC.constBottomSpaceMainSubtitle.constant = 0
            thisVC.constBottomSpaceTitle.constant = 0
        }

        if subSubtitle != nil {
            thisVC.lblSubSubtitle.text = subSubtitle?.trimmingCharacters(in: .whitespaces)
        } else {
            thisVC.lblSubSubtitle.isHidden = true
            thisVC.constHeightSubSubtitle.constant = 0
            thisVC.constBottomSpaceSubtitle.constant = 0
        }

        // Sets the default title of buttons
        if defaultTitle != nil {
            thisVC.btnDefault.setTitle(defaultTitle?.trimmingCharacters(in: .whitespaces), for: .normal)
            thisVC.btnDefaultOnly.setTitle(defaultTitle?.trimmingCharacters(in: .whitespaces), for: .normal)
        } else {
            thisVC.btnDefault.setTitle("OK", for: .normal)
            thisVC.btnDefaultOnly.setTitle("OK", for: .normal)
        }

        if destructiveTitle != nil {
            thisVC.btnDestructive.setTitle(destructiveTitle?.trimmingCharacters(in: .whitespaces), for: .normal)
        } else {
            thisVC.btnDestructive.setTitle("CANCEL", for: .normal)
        }
        
        // Present the dialog
        inVC.present(popup, animated: true, completion: onPresentComplete)
        
    }
}

