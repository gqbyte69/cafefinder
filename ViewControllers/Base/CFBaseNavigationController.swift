//
//  CFBaseNavigationController.swift
//  CafeFinder
//
//  Created by Adonis Dumadapat on 06/01/2018.
//  Copyright © 2018 A3T Software Development Services. All rights reserved.
//

import UIKit

class CFBaseNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationBar.barTintColor = UIColor.rgb(red: 33, green: 123, blue: 180)
        navigationBar.titleTextAttributes =
        [
            NSAttributedStringKey.font: UIFont.sbFont(withWeight: .BOLD, andSize: 19),
            NSAttributedStringKey.foregroundColor : UIColor.white
        ]
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }

}
