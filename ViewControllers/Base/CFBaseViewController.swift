//
//  CFBaseViewController.swift
//  CafeFinder
//
//  Created by Adonis Dumadapat on 06/01/2018.
//  Copyright © 2012-2018 A3T Software Development Services, Inc.
//

import Foundation
import UIKit

class CFBaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if let navCon = navigationController {
            navCon.setNavigationBarHidden(false, animated: true)
            navCon.navigationBar.isTranslucent = false
        }

        // This removes the text of the back button
        let backItem = UIBarButtonItem()
        backItem.title = ""
        backItem.tintColor = UIColor.white
        self.navigationItem.backBarButtonItem = backItem
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        view.endEditing(true)
    }

    // We use this to make sure that we always pop with a valid navigation controller
    func popViewController(){

        if let navCon = navigationController {
            let _ = navCon.popViewController(animated: true)
        }
        
    }
    
    // We use this to make sure that we always pop with a valid navigation controller
    func popToRootViewController(){

        if let navCon = navigationController {
            let _ = navCon.popToRootViewController(animated: true)
        }
    }
    
    // We use this to make sure that we always push with a valid navigation controller
    func pushVC(viewController: UIViewController) {
        
        if let navCon = navigationController {
            navCon.pushViewController(viewController, animated: true)
        }
        
        // We do nothing if we have no navigation controller
    }

}
