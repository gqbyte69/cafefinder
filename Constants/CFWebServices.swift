//
//  CFWebServices.swift
//  CafeFinder
//
//  Created by Adonis Dumadapat on 06/01/2018.
//  Copyright © 2012-2018 A3T Software Development Services, Inc.
//

// Keys and string literals
let CAFE            = "cafe"
let DATA            = "data"
let GEOMETRY        = "geometry"
let HEIGHT          = "height"
let ID              = "id"
let ICON            = "icon"
let KEY             = "key"
let KEYWORD         = "keyword"
let KJSON           = "json"
let LAT             = "lat"
let LNG             = "lng"
let LOCATION        = "location"
let MAXWIDTH        = "maxwidth"
let NAME            = "name"
let NEARBY          = "nearbysearch/"
let NORTHEAST       = "northeast"
let OK              = "ok"
let OPEN_NOW        = "open_now"
let OPENING_HOURS   = "opening_hours"
let PHOTO           = "photo"
let PHOTOS          = "photos"
let PHOTO_REFERENCE = "photo_reference"
let PHOTOREFERENCE  = "photoreference"
let PLACE           = "place/"
let PLACE_ID        = "place_id"
let PRICE_LEVEL     = "price_level"
let RADIUS          = "radius"
let RATING          = "rating"
let REFERENCE       = "reference"
let RESULTS         = "results"
let SCOPE           = "scope"
let SOUTHWEST       = "southwest"
let STATUS          = "status"
let SUCCESS         = "success"
let TYPE            = "type"
let TYPES           = "types"
let VIEWPORT        = "viewport"
let VICINITY        = "vicinity"
let WEEKDAY_TEXT    = "weekday_text"
let WIDTH           = "width"

// URL's
let PROTOCOL        = "https://"
let BASE            = "maps.googleapis.com/maps/api/"

let URL_NEARBY      = PROTOCOL + BASE + PLACE + NEARBY + KJSON
let URL_PHOTO       = PROTOCOL + BASE + PLACE + PHOTO

