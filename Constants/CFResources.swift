//
//  CFResources.swift
//  CafeFinder
//
//  Created by Adonis Dumadapat on 06/01/2018.
//  Copyright © 2012-2018 A3T Software Development Services, Inc.
//

import UIKit

struct SB {
    static let DASHBOARD = UIStoryboard(name: "Dashboard", bundle: nil)
}

struct VC {
    let DASHBOARD = SB.DASHBOARD.instantiateViewController(withIdentifier: "CFDashboardViewControllerID") as! CFDashboardViewController
    let DETAILS = SB.DASHBOARD.instantiateViewController(withIdentifier: "CFPlaceDetailsViewControllerID") as! CFPlaceDetailsViewController
}

struct IMG {
    static let PLACEHOLDER = UIImage(named: "placeholder")
    static let X_CIRCLE_G  = UIImage(named:"x_circle_green")!
}
