//
//  CFEnumerations.swift
//  CafeFinder
//
//  Created by Adonis Dumadapat on 06/01/2018.
//  Copyright © 2012-2018 A3T Software Development Services, Inc.
//

import Foundation

enum REST_VERBS {
    case POST
    case GET
    case PUT
    case DELETE
}

enum GRADIENT_COLORS {
    case GREEN
    case RED
}

enum FONT_WEIGHT {
    case REGULAR
    case BOLD
    case SEMI_BOLD
    case LIGHT
}


