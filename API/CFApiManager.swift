//
//  CFApiManager.swift
//  CafeFinder
//
//  Created by Adonis Dumadapat on 06/01/2018.
//  Copyright © 2018 A3T Software Development Services. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

typealias GenericCompletion = (() -> Swift.Void)?
typealias ApiCompletion     = ((_ success: Bool, _ message: String, _ data: Any?) -> Swift.Void)?
typealias OnSuccessHandler  = ((_ data: JSON) -> Swift.Void)?
typealias OnFailHandler     = ((_ data: CFApiErrorModel) -> Swift.Void)?

class CFApiManager: NSObject {

    static let sharedInstance : CFApiManager = {
        let instance = CFApiManager()
        return instance
    }()

    override init() {

    }

    //====================================================================================================================================================================================================================
    // MARK: - Public static functions -
    //====================================================================================================================================================================================================================
    class func dictToParam(dict: NSDictionary, isGet get: Bool? = true) -> String {
        var returnString = ""

        if get! {

            returnString = returnString + "?"
            for (key, value) in dict {
                returnString = "\(returnString)\(key)=\(value)&"
            }
        } else {
            for (key, value) in dict {
                returnString = "\(returnString)\(key)\\\(value)\\"
            }
        }

        // Remove the last character
        returnString.remove(at: returnString.index(before: returnString.endIndex))

        return returnString
    }

    //====================================================================================================================================================================================================================
    // MARK: - Internal private functions -
    //====================================================================================================================================================================================================================
    fileprivate func retry(
        numberOfTimes: Int,
        loadingTitle title: String? = nil,
        task: @escaping (_ success: @escaping (JSON) ->(), _ failure: @escaping (String, Int) -> ()) -> Void,
        success: OnSuccessHandler = nil,
        failure: OnFailHandler = nil
        )
    {

        if let loadTitle = title {
            CFProgressIndicator.show(withMessage: loadTitle)
        }

        task({ (response) in
            CFProgressIndicator.dismiss()
            success?(response)
        }) { (error, code) in

            var counter = numberOfTimes
            if counter > 1 {
                log.error("RETRYING....(\(numberOfTimes))")

                self.retry(numberOfTimes: counter - 1, task: task, success: success, failure: failure)
            } else {
                log.error("RETRY COUNT MAXED.")

                CFProgressIndicator.dismiss()

                CFAlert.genericChoice(
                    message: error,
                    confirmText: "alert_retry".localized(),
                    onYes: {
                        counter = 3
                        self.retry(numberOfTimes: counter - 1, task: task, success: success, failure: failure)
                },
                    onNo: {
                        let errorObjc: CFApiErrorModel = CFApiErrorModel()
                        errorObjc.message = error
                        errorObjc.code = code
                        failure?(errorObjc)
                })
            }
        }
    }

    internal func consumeAPI(
        usingUrl url: String,
        withVerb restVerb: REST_VERBS,
        withTitle title: String? = nil,
        withParameters params: NSDictionary? = nil,
        onSuccess: OnSuccessHandler = nil,
        onFail: OnFailHandler = nil
        ) {

        let uuid = UUID().uuidString

        var localVerb : HTTPMethod!

        switch restVerb {
        case .POST:
            localVerb = .post
            break
        case .GET:
            localVerb = .get
            break
        case .PUT:
            localVerb = .put
            break
        case .DELETE:
            localVerb = .delete
            break
        }

        if params != nil {
            log.debug("\n\nVERB: [\(restVerb)]\nURL:  [\(url)]\nREQUEST: [\(uuid)]: \nPARAMS:  [\(params!)]\n")
        } else {
            log.debug("\n\nVERB: [\(restVerb)]\nURL:  [\(url)]\nREQUEST: [\(uuid)]: \nPARAMS:  [none]\n")
        }

        var requestParam : Parameters? = nil

        if let parameters = params {
            let res : JSON = JSON.init(parameters)

            var dict = [String:Any]()
            for (key, value) in res {
                dict[key] = value.rawString()
            }

            requestParam = dict
        }

        // Conventional API
        self.retry(
            numberOfTimes: 3,
            loadingTitle: title,
            task: { (onSuccess, onFail) in
                Alamofire.request(
                    url,
                    method: localVerb,
                    parameters: requestParam,
                    encoding: URLEncoding.default,
                    headers: nil).responseJSON { response in

                        let responseDataString = NSString(data: response.data!, encoding:String.Encoding.utf8.rawValue)
                        print("\n\nRESPONSE [\(uuid)]: \n\(responseDataString!)\n")

                        switch response.result {

                        case .success(_):
                            if response.result.value is NSDictionary {
                                let value = response.result.value as! NSDictionary
                                onSuccess(JSON(value))
                            } else {
                                let dict: NSMutableDictionary = NSMutableDictionary()
                                dict.setValue(response.result.value, forKey: DATA)
                                onSuccess(JSON(dict))
                            }
                            break

                        case .failure(let error):

                            if let err = error as? URLError {

                                switch err.code {
                                case URLError.Code.notConnectedToInternet:
                                    // No internet connection
                                    onFail("error_no_internet".localized(), 500)
                                    break
                                case URLError.Code.timedOut:
                                    onFail("error_request_timeout".localized(), 500)
                                    break
                                default:
                                    onFail("Server error: (\(err.errorCode))", err.errorCode)
                                    break
                                }

                            } else {
                                // Other failures
                                log.error("ERROR [\(uuid)]: \(response.result.error!)\n\nACTUAL DATA: \(responseDataString ?? "")")
                                onFail("error_invalid_server_response".localized(), 9999)
                            }

                            break
                        }
                }
        },
            success: onSuccess,
            failure: onFail
        )

    }
}


