//
//  CFApi.swift
//  CafeFinder
//
//  Created by Adonis Dumadapat on 06/01/2018.
//  Copyright © 2018 A3T Software Development Services. All rights reserved.
//

import SwiftyJSON
import CoreLocation

class CFApi: NSObject {

    class func getNearby(
        withKeyword keyword: String? = nil,
        withCoordinate coordinate: CLLocation,
        onSuccess: OnSuccessHandler = nil,
        onFail: OnFailHandler = nil
        )
    {

        let locString: String = "\(coordinate.coordinate.latitude),\(coordinate.coordinate.longitude)"
        let params: NSMutableDictionary =
        [
            LOCATION: locString,
            RADIUS: DEFAULT_RADIUS,
            TYPE: CAFE,
            KEY: GOOGLE_PLACES_KEY
        ];

        if let kw = keyword {
            params.setValue(kw, forKey: KEYWORD)
        }

        CFApiManager.sharedInstance.consumeAPI(
            usingUrl: URL_NEARBY,
            withVerb: .GET,
            withTitle: "load_fetching".localized(),
            withParameters: params,
            onSuccess: { (data) in
                onSuccess?(data)
        }) { (errorData) in
            onFail?(errorData)
        }
    }

}

class CFApiWrapper: NSObject {

    class func getNearby(
        withKeyword keyword: String? = nil,
        withCoordinate coordinate: CLLocation,
        onComplete: ApiCompletion = nil
        )
    {
        CFApi.getNearby(
            withKeyword: keyword,
            withCoordinate: coordinate,
            onSuccess: { (data) in
                let response: CFApiResponseModel = CFApiResponseModel.init(data: data)

                if !response.status {
                    onComplete?(false, response.statusMessage, nil)
                } else {
                    onComplete?(true, SUCCESS, response.results)
                }
        }) { (errorData) in
            onComplete?(false, errorData.message, nil)
        }
    }

}
