//
//  CFProgressIndicator.swift
//  CafeFinder
//
//  Created by Adonis Dumadapat on 06/01/2018.
//  Copyright © 2018 A3T Software Development Services. All rights reserved.
//

import SVProgressHUD

class CFProgressIndicator: NSObject {

    /**
     Shows a loading screen that blocks all user interaction to the screen

     - Parameters:
     - message: Optional message to show.
     - Note: All strings will be localized so DON'T add .localized() on the parameter
     */
    class func show(withMessage message: String = ""){
        SVProgressHUD.show(withStatus: message.localized())
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.light)
    }

    class func dismiss() {
        SVProgressHUD.dismiss()
    }
}
