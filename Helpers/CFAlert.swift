//
//  CFAlert.swift
//  CafeFinder
//
//  Created by Adonis Dumadapat on 06/01/2018.
//  Copyright © 2012-2018 A3T Software Development Services, Inc.
//

import UIKit

class CFAlert: NSObject {

    class func error(
        in vc: UIViewController? = UIApplication.topViewController(),
        withTitle title: String,
        withMessage message: String? = nil,
        onComplete: (() -> Swift.Void)? = nil
        ) {
        CFGenericDialogViewController.showDialog(
            inVC: vc!,
            iconImage: IMG.X_CIRCLE_G,
            title: title,
            defaultOnly: true,
            mainSubtitle: message,
            subSubtitle: nil,
            defaultTitle: "OK",
            destructiveTitle: nil,
            onPresentComplete: nil,
            defaultAction: onComplete,
            destructiveAction: nil)
    }

    class func message(
        in vc: UIViewController? = UIApplication.topViewController(),
        withTitle title: String,
        withMessage message: String? = nil,
        onComplete: (() -> Swift.Void)? = nil
        ) {
        CFGenericDialogViewController.showDialog(
            inVC: vc!,
            iconImage: nil,
            title: title,
            defaultOnly: true,
            mainSubtitle: message,
            subSubtitle: nil,
            defaultTitle: "OK",
            destructiveTitle: nil,
            onPresentComplete: nil,
            defaultAction: onComplete,
            destructiveAction: nil)
    }

    class func genericChoice(
        in vc: UIViewController? = UIApplication.topViewController(),
        message title: String = "",
        confirmText confirm: String? = "alert_ok".localized(),
        destructiveText cancel: String? = "alert_cancel".localized(),
        onYes: (() -> Swift.Void)? = nil,
        onNo: (() -> Swift.Void)? = nil)
    {

        CFGenericDialogViewController.showDialog(
            inVC: vc!,
            iconImage: nil,
            title: title,
            defaultOnly: false,
            mainSubtitle: nil,
            subSubtitle: nil,
            defaultTitle: confirm,
            destructiveTitle: nil,
            onPresentComplete: nil,
            defaultAction: onYes,
            destructiveAction: onNo)

    }
}

