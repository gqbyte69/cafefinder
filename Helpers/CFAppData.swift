//
//  CFAppData.swift
//  CafeFinder
//
//  Created by Adonis Dumadapat on 06/01/2018.
//  Copyright © 2012-2018 A3T Software Development Services, Inc.
//

import Foundation

class CFAppData: NSObject {
    
    var version: String!

    static let sharedInstance : CFAppData = {
        let instance = CFAppData()
        return instance
    }()

    
    override init(){
        let object: AnyObject? = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as AnyObject?
        version = object as! String
    }
    
}
