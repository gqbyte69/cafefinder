//
//  CFPhotoModel.swift
//  CafeFinder
//
//  Created by Adonis Dumadapat on 06/01/2018.
//  Copyright © 2018 A3T Software Development Services. All rights reserved.
//

import SwiftyJSON

class CFPhotoModel: CFBaseModel {

    var height: Double!
    var width: Double!
    var photoReference: String!

    init(data: JSON) {
        height         = data[HEIGHT].doubleValue
        width          = data[WIDTH].doubleValue
        photoReference = data[PHOTO_REFERENCE].string!
    }

    func photUrl(withWidth targetWidth: CGFloat) -> String {

        let widthString: String = targetWidth.description
        let arrayParts: [String] = widthString.components(separatedBy: ".")

        let params: NSDictionary =
        [
            MAXWIDTH: arrayParts.first!,
            PHOTOREFERENCE: photoReference,
            KEY: GOOGLE_PLACES_KEY
        ]

        let paramString: String = CFApiManager.dictToParam(dict: params)

        return URL_PHOTO + paramString
    }
}
