//
//  CFApiErrorModel.swift
//  CafeFinder
//
//  Created by Adonis Dumadapat on 06/01/2018.
//  Copyright © 2018 A3T Software Development Services. All rights reserved.
//

import Foundation

class CFApiErrorModel: CFBaseModel {

    var message: String!
    var code: Int!

}
