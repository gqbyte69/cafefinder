//
//  CFApiResponseModel.swift
//  CafeFinder
//
//  Created by Adonis Dumadapat on 06/01/2018.
//  Copyright © 2018 A3T Software Development Services. All rights reserved.
//

import SwiftyJSON

class CFApiResponseModel: CFBaseModel {

    var htmlAttributions: JSON!
    var nextPageToken: String!
    var results: [CFPlaceModel]! = []
    var status: Bool!
    var statusMessage: String!

    override init() {
        super.init()

    }

    init(data: JSON) {
        if data[RESULTS].exists() {
            for obj in data[RESULTS].array! {
                let place: CFPlaceModel = CFPlaceModel.init(data: obj)
                results.append(place)
            }
        }

        statusMessage = data[STATUS].string!

        if statusMessage.lowercased() == OK {
            status = true
        } else {
            status = false
        }
    }
}
