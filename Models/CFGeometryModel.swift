//
//  CFGeometryModel.swift
//  CafeFinder
//
//  Created by Adonis Dumadapat on 06/01/2018.
//  Copyright © 2018 A3T Software Development Services. All rights reserved.
//

import SwiftyJSON
import CoreLocation

class CFGeometryModel: CFBaseModel {

    var location: CLLocation!
    var viewPort: CFViewPortModel!

    init(data: JSON) {

        if data[LOCATION].exists() {
            let loc: JSON = data[LOCATION]
            location = CLLocation(latitude: loc[LAT].doubleValue, longitude: loc[LNG].doubleValue)
        }

        if data[VIEWPORT].exists() {
            viewPort = CFViewPortModel.init(data: data[VIEWPORT])
        }
    }
}
