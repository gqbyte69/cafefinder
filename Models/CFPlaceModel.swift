//
//  CFPlaceModel.swift
//  CafeFinder
//
//  Created by Adonis Dumadapat on 06/01/2018.
//  Copyright © 2018 A3T Software Development Services. All rights reserved.
//

import Foundation
import SwiftyJSON

class CFPlaceModel: CFBaseModel {

    var geometry: CFGeometryModel!
    var icon: String!
    var id: String!
    var name: String!
    var openingHours: CFOpeningHoursModel!
    var photos: [CFPhotoModel]! = []
    var placeId: String!
    var priceLevel: String!
    var rating: String!
    var reference: String!
    var scope: String!
    var types: [String]! = []
    var vicinity: String!

    init(data: JSON) {
        icon = data[ICON].string
        id = data[ID].string
        name = data[NAME].string
        placeId = data[PLACE_ID].string
        priceLevel = data[PRICE_LEVEL].string
        rating = data[RATING].string
        reference = data[REFERENCE].string
        scope = data[SCOPE].string
        vicinity = data[VICINITY].string

        if data[GEOMETRY].exists() {
            let geo: JSON = data[GEOMETRY]
            geometry = CFGeometryModel.init(data: geo)
        }

        if data[OPENING_HOURS].exists() {
            let oh: JSON = data[OPENING_HOURS]
            openingHours = CFOpeningHoursModel.init(data: oh)
        }

        if data[TYPES].exists() {
            for obj in data[TYPES].array! {
                types.append(obj.stringValue)
            }
        }

        if data[PHOTOS].exists() {
            for obj in data[PHOTOS].array! {
                let pm: CFPhotoModel = CFPhotoModel.init(data: obj)
                photos.append(pm)
            }
        }
    }
}
