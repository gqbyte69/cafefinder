//
//  CFOpeningHoursModel.swift
//  CafeFinder
//
//  Created by Adonis Dumadapat on 06/01/2018.
//  Copyright © 2018 A3T Software Development Services. All rights reserved.
//

import SwiftyJSON

class CFOpeningHoursModel: CFBaseModel {

    var openNow: Bool!
    var weekeDayText: [String]!

    init(data: JSON) {
        openNow = data[OPEN_NOW].boolValue

        if data[WEEKDAY_TEXT].exists() {
            for obj in data[WEEKDAY_TEXT].array! {
                weekeDayText.append(obj.stringValue)
            }
        }
    }
}
