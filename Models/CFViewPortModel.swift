//
//  CFViewPortModel.swift
//  CafeFinder
//
//  Created by Adonis Dumadapat on 06/01/2018.
//  Copyright © 2018 A3T Software Development Services. All rights reserved.
//

import SwiftyJSON
import CoreLocation

class CFViewPortModel: CFBaseModel {

    var northEast: CLLocation!
    var southWest: CLLocation!

    init(data: JSON) {

        if data[NORTHEAST].exists() {
            let vp: JSON = data[NORTHEAST]
            northEast = CLLocation(latitude: vp[LAT].doubleValue, longitude: vp[LNG].doubleValue)
        }

        if data[SOUTHWEST].exists() {
            let vp: JSON = data[SOUTHWEST]
            southWest = CLLocation(latitude: vp[LAT].doubleValue, longitude: vp[LNG].doubleValue)
        }

    }
}
