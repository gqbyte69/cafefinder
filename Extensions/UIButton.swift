//
//  UIButton.swift
//  CafeFinder
//
//  Created by Adonis Dumadapat on 06/01/2018.
//  Copyright © 2012-2018 A3T Software Development Services, Inc.
//

import UIKit

extension UIButton {

    @IBInspectable var localizationKey: String {
        set {
            if newValue != "" {
                self.setTitle(newValue.localized(), for: .normal)
            }
        }
        get {
            return self.titleLabel?.text ?? ""
        }

    }

}

