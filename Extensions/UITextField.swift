//
//  UITextField.swift
//  CafeFinder
//
//  Created by Adonis Dumadapat on 06/01/2018.
//  Copyright © 2012-2018 A3T Software Development Services, Inc.
//

import UIKit

extension UITextField {
    
    func cleanText() -> String {
        guard let newText = text else {
            return ""
        }
        
        return newText.trimmingCharacters(in: .whitespaces)
    }

    func clear() {
        text = ""
    }

}
