//
//  UIColor.swift
//  CafeFinder
//
//  Created by Adonis Dumadapat on 06/01/2018.
//  Copyright © 2012-2018 A3T Software Development Services, Inc.
//

import UIKit

extension UIColor {

    static let pcGreen = UIColor(red: 84.0/255.0, green: 131.0/255.0, blue: 53.0/255.0, alpha: 1.0)
    static let pcOrange = UIColor(red: 255.0/255.0, green: 140.0/255.0, blue: 17.0/255.0, alpha: 1.0)
    static let pcGray = UIColor(red: 158.0/255.0, green: 158.0/255.0, blue: 158.0/255.0, alpha: 1.0)

    class func rgb(red redValue: CGFloat, green greenValue: CGFloat, blue blueValue: CGFloat) -> UIColor {
        return UIColor(red: redValue/255.0, green: greenValue/255.0, blue: blueValue/255.0, alpha: 1.0)
    }
}
