//
//  UIView.swift
//  CafeFinder
//
//  Created by Adonis Dumadapat on 06/01/2018.
//  Copyright © 2012-2018 A3T Software Development Services, Inc.
//

import UIKit

extension UIView {

    @IBInspectable var isBordered:Bool {
        set {
            if newValue {
                self.layer.borderWidth = 1
                self.layer.borderColor = UIColor.white.cgColor
            }
        }
        get {
            return self.layer.borderWidth == 1
        }
    }
    
    @IBInspectable var isRoundedCorner: Bool {
        set {
            if newValue {
                self.layer.cornerRadius = 5
            }
        }
        get {
            return self.layer.cornerRadius > 0
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        set {
            if newValue != cornerRadius {
                self.layer.cornerRadius = newValue
            }
        }
        get {
            return self.layer.cornerRadius
        }
    }
    
    @IBInspectable var borderColor: UIColor {
        set {
            if newValue != borderColor {
                self.layer.borderColor = newValue.cgColor
            }
        }
        get {
            return UIColor(cgColor: self.layer.borderColor!)
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        set {
            if newValue != borderWidth {
                self.layer.borderWidth = newValue
            }
        }
        get {
            return self.layer.borderWidth
        }
    }

    @IBInspectable var hasShadow: Bool {
        set {
            if newValue {
                self.addShadow()
            }
        }
        get {
            return !self.layer.masksToBounds
        }
    }

    func animate(withDuration: TimeInterval) {
        UIView.animate(withDuration: withDuration) {
            self.layoutIfNeeded()
        }
    }

    func addShadow() {
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowRadius = 2
        self.layer.shadowOpacity = 0.7
    }

    func removeAllConstraints() {
        self.removeConstraints(self.constraints)
        for view in self.subviews {
            view.removeAllConstraints()
        }
    }
}
