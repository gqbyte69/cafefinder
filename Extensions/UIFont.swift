//
//  UIFont.swift
//  CafeFinder
//
//  Created by Adonis Dumadapat on 06/01/2018.
//  Copyright © 2012-2018 A3T Software Development Services, Inc.
//

import UIKit

extension UIFont {

    class func sbFont(withWeight weight: FONT_WEIGHT, andSize size: CGFloat) -> UIFont! {
        switch weight {
        case .REGULAR:
            return UIFont(name: "SourceSansPro-Regular", size: size)!
        case .BOLD:
            return UIFont(name: "SourceSansPro-Bold", size: size)!
        case .LIGHT:
            return UIFont(name: "SourceSansPro-Light", size: size)!
        case .SEMI_BOLD:
            return UIFont(name: "SourceSansPro-Semibold", size: size)!
        }
    }
}
