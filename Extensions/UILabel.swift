//
//  UILabel.swift
//  CafeFinder
//
//  Created by Adonis Dumadapat on 06/01/2018.
//  Copyright © 2012-2018 A3T Software Development Services, Inc.
//

import UIKit

extension UILabel {

    @IBInspectable var localizationKey: String {
        set {
            if newValue != "" {
                self.text = newValue.localized()
            }
        }
        get {
            return self.text ?? ""
        }

    }

}
