//
//  String.swift
//  CafeFinder
//
//  Created by Adonis Dumadapat on 06/01/2018.
//  Copyright © 2012-2018 A3T Software Development Services, Inc.
//

extension String {

    subscript (i: Int) -> Character {
        return self[index(startIndex, offsetBy: i)]
    }

    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }

    subscript (r: Range<Int>) -> String {
        let start = index(startIndex, offsetBy: r.lowerBound)
        let end = index(startIndex, offsetBy: r.upperBound - r.lowerBound)
        return String(self[Range(start ..< end)])
    }
}
